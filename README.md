# msisdn-golang-vagrant-ansible-docker

# Guide


## Requirements


### Install the tools


* VirtualBox 5.1.28
* Vagrant 2.0.0
* Git


### Install vagrant plugins using the commands:


```
vagrant plugin install vagrant-vbguest
```

```
vagrant plugin install vagrant-hostmanager
```


## Use the application

* Start VirtualBox as admin
* Start the command line tool (git-bash) as admin

* Navigate to root of repository where vagrant file is located

* Start vagrant
```
vagrant up
```

* Wait for the setup to finish, usually 15 min
* Go to http://10.0.100.10:8080, enter the mobile phone and use the Send button


## Additional commands

* Ssh into machine
```
vagrant ssh base
```

* Shutdown vagrant
```
vagrant halt
```

* Destroy vagrant machines
```
vagrant destroy -f
```
