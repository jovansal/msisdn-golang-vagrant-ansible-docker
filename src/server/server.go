package main

import (
	"server/msisdnparser"
	"server/helpers"
	"io"
	"log"
	"net/http"
	"net/rpc"
	"fmt"
)

type Arg string
type MsisdnRpc int

func (t *MsisdnRpc) Parse(msisdn *Arg, data *msisdnparser.MsisdnPayload) error {
	m := msisdnparser.Parse(string(*msisdn))
	
	*data = m
	return nil
}

func main() {
	msisdnRpc := new(MsisdnRpc)
	rpc.Register(msisdnRpc)
	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		fmt.Println("req.body");
		fmt.Println(req.Body);

		defer req.Body.Close()
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		res := helpers.NewRPCRequest(req.Body).Call()
		io.Copy(w, res)
	})
	log.Fatal(http.ListenAndServe(":3000", nil))
}