package msisdnparser

import (
	"strings"
)

func generateMsisdnPayload(number string) MsisdnPayload {
	country, countryCode := getCountryFromCountryCode(number)
	mnoProvider, subscriberNumber := getMNOProvider(number)

	if subscriberNumber == "" {
		subscriberNumber = number[len(countryCode):]
	}
	return MsisdnPayload{CDC: countryCode, SN: subscriberNumber, CI: country, MNO: mnoProvider}
}

func removeExtraCharacters(msisdnString string) string {
	var cleanNumber string;

	cleanNumber = strings.TrimLeft(msisdnString, " 0")

	cleanNumber = strings.Replace(cleanNumber, "(", "", -1)
	cleanNumber = strings.Replace(cleanNumber, ")", "", -1)
	cleanNumber = strings.Replace(cleanNumber, "-", "", -1)

	cleanNumber = strings.Replace(cleanNumber, " ", "", -1)
	cleanNumber = strings.Replace(cleanNumber, "	", "", -1)

	cleanNumber = strings.TrimLeft(cleanNumber, "+")

	return cleanNumber;
}
