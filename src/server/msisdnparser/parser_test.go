package msisdnparser

import (
	"testing"
)

func TestParse(t *testing.T) {
	msisdnPayload := Parse("+389 7s 222 333")
	if (msisdnPayload.ERR != numberWithNotOnlyNumbers ) {
		t.Errorf("Expected error \"%v\" but got \"%v\" ", numberWithNotOnlyNumbers, msisdnPayload.ERR);
	}

	msisdnPayload = Parse("+389 75 222 333")
	if ( msisdnPayload.CDC != "389" || msisdnPayload.CI != "MK" || msisdnPayload.MNO != "OneVip" || msisdnPayload.SN != "222333") {
		t.Errorf("Expected { CDC:389, CI: MK, MNO: OneVip, SN: 222333 } \n but got: CDC: %v, CI: %v, MNO: %v, SN: %v",
			msisdnPayload.CDC, msisdnPayload.CI, msisdnPayload.MNO, msisdnPayload.SN )
	}
	
}