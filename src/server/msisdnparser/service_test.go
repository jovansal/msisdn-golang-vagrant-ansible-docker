package msisdnparser

import (
	"testing"
)

func TestGenerateMsisdnPayload(t *testing.T) { 
	msisdnPayload := generateMsisdnPayload("38975606707")
	if (msisdnPayload.CDC != "389" || msisdnPayload.CI != "MK" || msisdnPayload.MNO != "OneVip" || msisdnPayload.SN != "606707") {
		t.Errorf("Expected { CDC:389, CI: MK, MNO: OneVip, SN: 606707 } \n but got: CDC: %v, CI: %v, MNO: %v, SN: %v",
			msisdnPayload.CDC, msisdnPayload.CI, msisdnPayload.MNO, msisdnPayload.SN )
	}

	msisdnPayload = generateMsisdnPayload("389 75606707")
	if (msisdnPayload.CDC != "389" || msisdnPayload.CI != "MK" || msisdnPayload.MNO != "Unknown provider" || msisdnPayload.SN != " 75606707") {
		t.Errorf("Expected { CDC:389, CI: MK, MNO: Unknown provider, SN: 75606707 } \n but got: CDC: %v, CI: %v, MNO: %v, SN: %v",
			msisdnPayload.CDC, msisdnPayload.CI, msisdnPayload.MNO, msisdnPayload.SN )
	}
}

func TestRemoveExtraCharacters(t *testing.T) { 
	number := removeExtraCharacters("+38975500600")
	if (number != "38975500600") {
		t.Errorf("Expected 38975500600 but got: %v", number)
	}

	number = removeExtraCharacters("  +38975500600  ")
	if (number != "38975500600") {
		t.Errorf("Expected 38975500600 but got: %v", number)
	}

	number = removeExtraCharacters("  0038975500600  ")
	if (number != "38975500600") {
		t.Errorf("Expected 38975500600 but got: %v", number)
	}

	number = removeExtraCharacters("  00 389 75 500600  ")
	if (number != "38975500600") {
		t.Errorf("Expected 38975500600 but got: %v", number)
	}

	number = removeExtraCharacters("  + (389) (75) 500600  ")
	if (number != "38975500600") {
		t.Errorf("Expected 38975500600 but got: %v", number)
	}
}