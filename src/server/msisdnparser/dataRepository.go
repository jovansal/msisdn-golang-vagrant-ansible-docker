package msisdnparser

func findMNOProviders(number string) (string, bool) {
	i, present := mnoProviders[number]
	return i, present
}

func findCountry(s string) (string, bool){
	res, err := countries[s]
	return res, err
}
