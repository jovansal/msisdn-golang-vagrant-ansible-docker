package msisdnparser

func Parse(msisdnString string) MsisdnPayload {
	cleanMsisdnString := removeExtraCharacters(msisdnString)

	msisdnPayloadWithError, validationError := validateNumber(cleanMsisdnString)
	if(validationError){
		return msisdnPayloadWithError;
	}

	return generateMsisdnPayload(cleanMsisdnString);
}
