package msisdnparser

const unknownProvider = "Unknown provider"

func getCountryFromCountryCode(s string) (string, string) {
	limit := 5 

	if len(s) < 5 {
		limit = len(s) + 1
	}

	var country string
	var countryCode string
	for i := 1; i < limit; i++ {
		if cc, exists := findCountry(s[:i]); exists {
			country = cc
			countryCode = s[:i]
			break;
		}
		
	}
	if(country == "") {
		return "", ""
	}

	return country, countryCode
}

func getMNOProvider(s string) (string, string) {
	var mno = unknownProvider
	var subscriberNumber = ""
	limit := 8 

	if len(s) < 8 {
		limit = len(s) + 1
	}

	for i := 1; i < limit; i++ {
		if mnoString, ok := findMNOProviders(s[:i]); ok {
			mno = mnoString
			subscriberNumber = s[i:]
			break;
		}
	}

	return mno, subscriberNumber;
}