package msisdnparser

type MsisdnPayload struct {
	CDC string
	SN string
	CI string
	MNO string
	ERR string
}