package msisdnparser

import (
	"testing"
)

func TestGetCountryFromCountryCode(t *testing.T) {
	
	country, countryCode := getCountryFromCountryCode("38975345")
	if (country != "MK" && countryCode != "389" ) {
		t.Errorf("Expected country MK and country code 389 but got: %v %v", country, countryCode)
	}

	country, countryCode = getCountryFromCountryCode("389")
	if (country != "MK" && countryCode != "389" ) {
		t.Errorf("Expected country MK and country code 389 but got: %v %v", country, countryCode)
	}

	country, countryCode = getCountryFromCountryCode("3")
	if (country != "" && countryCode != "" ) {
		t.Errorf("Expected empty country and country code but got: %v %v", country, countryCode)
	}

	country, countryCode = getCountryFromCountryCode("")
	if (country != "" && countryCode != "" ) {
		t.Errorf("Expected empty country and country code but got: %v %v", country, countryCode)
	}
}

func TestGetMNOProvider(t *testing.T) {

	mno, subscriberNumber := getMNOProvider("38975851233")
	if (mno != "OneVip" && subscriberNumber != "851233" ) {
		t.Errorf("Expected provider OneVip and subscriberNumber 851233 but got: %v %v", mno, subscriberNumber)
	}

	mno, subscriberNumber = getMNOProvider("38989851233")
	if (mno != unknownProvider && subscriberNumber != "" ) {
		t.Errorf("Expected Unknown provider and empty subscriberNumber but got: %v %v", mno, subscriberNumber)
	}

	mno, subscriberNumber = getMNOProvider("38911")
	if (mno != unknownProvider) {
		t.Errorf("Expected Unknown provider but got: %v", mno)
	}
	
	mno, subscriberNumber = getMNOProvider("")
	if (mno != unknownProvider) {
		t.Errorf("Expected Unknown provider but got: %v", mno)
	}
}