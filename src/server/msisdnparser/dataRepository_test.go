package msisdnparser

import (
	"testing"
)

func TestFindCountryWithExistingData(t *testing.T) {
	country, exist := findCountry("384")
	if(exist && country != "MK") {
		t.Errorf("Expected to return MK but returned %v", country);
	}
}

func TestFindCountryWithNonExistingData(t *testing.T) {
	_, exist := findCountry("189")
	if(exist) {
		t.Error("Expected to not find country with invalid code 189");
	}
}

func TestFindMNOProvidersWithExistingData(t *testing.T) {
	mnoProvider, exist := findMNOProviders("38975")
	if(exist && mnoProvider != "OneVip") {
		t.Errorf("Expected to return OneVip but returned %v", mnoProvider);
	}
}

func TestFindMNOProvidersWithNotExistingData(t *testing.T) {
	_, exist := findMNOProviders("31975")
	if(exist == true) {
		t.Error("Expected to not find MNO with invalid code 31975");
	}
}