package msisdnparser

import (
	"testing"
)

func TestValidateNumber(t *testing.T) {
	msisdnPayload, error := validateNumber("3897ss00600")
	if (msisdnPayload.ERR != numberWithNotOnlyNumbers || error == false ) {
		t.Errorf("Expected error \"%v\" but got \"%v\"",numberWithNotOnlyNumbers, msisdnPayload.ERR);
	}

	msisdnPayload, error = validateNumber("12375500600")
	if (msisdnPayload.ERR != countryCodeNotRecognized || error == false ) {
		t.Errorf("Expected error \"%v\" but got \"%v\"",countryCodeNotRecognized, msisdnPayload.ERR);
	}

	msisdnPayload, error = validateNumber("38975500600")
	if (msisdnPayload.ERR != "" || error == true ) {
		t.Errorf("Expected empty error but got \"%v\"", msisdnPayload.ERR);
	}
 }


 func TestValidateNumberContent(t *testing.T) {
	msisdnPayload := validateNumberContent("1234567")
	if (msisdnPayload.ERR != numberTooShort) {
		t.Errorf("Expect \"%v\" but got \"%v\"", numberTooShort, msisdnPayload.ERR );
	}

	msisdnPayload = validateNumberContent("1234567891234567")
	if (msisdnPayload.ERR != numberTooLong) {
		t.Errorf("Expect \"%v\" but got \"%v\"", numberTooLong, msisdnPayload.ERR );
	}

	msisdnPayload = validateNumberContent("e8975s00600")
	if (msisdnPayload.ERR != numberWithNotOnlyNumbers) {
		t.Errorf("Expect \"%v\" but got \"%v\"", numberWithNotOnlyNumbers, msisdnPayload.ERR );
	}

	msisdnPayload = validateNumberContent("38975500600")
	if (msisdnPayload.ERR != "") {
		t.Errorf("Expect empty error but got \"%v\"", msisdnPayload.ERR );
	}
}

func TestValidateCountryCode(t *testing.T) {
	msisdnPayload := validateCountryCode("12370200122")
	if (msisdnPayload.ERR != countryCodeNotRecognized) {
		t.Errorf("Expect \"%v\" but got \"%v\"", countryCodeNotRecognized, msisdnPayload.ERR );
	}

	msisdnPayload = validateCountryCode("38975500600")
	if (msisdnPayload.ERR != "") {
		t.Errorf("Expect empty error but got \"%v\"", msisdnPayload.ERR );
	}
}