package msisdnparser

import (
	"regexp"
)

const numberTooShort = "The number number is too short"
const numberTooLong = "The number is too long"
const numberWithNotOnlyNumbers = "The number does not contain only numbers"
const countryCodeNotRecognized = "The country code is not recognized"

func validateNumber(number string) (MsisdnPayload, bool) {
	msisdnPayloadFromValidateNumberContent := validateNumberContent(number)
	if (msisdnPayloadFromValidateNumberContent.ERR != "") {
		return msisdnPayloadFromValidateNumberContent, true
	}

	msisdnPayloadFromValidateCountryCode := validateCountryCode(number)
	if (msisdnPayloadFromValidateCountryCode.ERR != "") {
		return msisdnPayloadFromValidateCountryCode, true
	}

	return MsisdnPayload{ ERR:"" }, false
}

func validateNumberContent (number string) MsisdnPayload {

	if(len(number) < 8) {
		return MsisdnPayload{ERR: numberTooShort}
	}
	if(len(number) > 15) {
		return MsisdnPayload{ERR: numberTooLong}
	}

	var re = regexp.MustCompile(`^\d+$`)
	var testNumbers = re.MatchString(number)
	if(!testNumbers) {
		return MsisdnPayload{ERR: numberWithNotOnlyNumbers}
	}

	return MsisdnPayload{ERR: ""}
}

func validateCountryCode (number string) MsisdnPayload {
	country, countryCode := getCountryFromCountryCode(number)
	if(country == "" || countryCode == "") {
		return MsisdnPayload{ERR: countryCodeNotRecognized}
	}
	
	return MsisdnPayload{ERR: ""}
}
