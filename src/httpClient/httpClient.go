package main

import (
        "net/http"
)

func main() {
        http.Handle("/", http.FileServer(http.Dir("/go/src/client/public")))
        http.ListenAndServe(":8080", nil)
}